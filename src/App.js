import React from "react";
import Home from './Home';

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import appReducer from '../reducers/appReducer'

const store = createStore(appReducer);

export default () => (
  <Provider store={store}>
    <Home />
  </Provider>
);
