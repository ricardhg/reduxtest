import React from 'react';
import {connect} from 'react-redux';

class Mostrar extends React.Component {
    
    render() { 

        if (!this.props.elements.length) {
            return <h3>No data...</h3>;
        }
        
        let i = 1;
        let lis = this.props.elements.map(el => <li key={i++}>{el}</li>);

        return ( 
            <>
                <ul>
                    {lis}
                </ul>
            </>
         );
    }
}
 
const mapStateToProps = (state) => {
    return {
       elements: state.elements
    }
  }
  
export default connect(mapStateToProps)(Mostrar);
  
  
