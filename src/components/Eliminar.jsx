
import React from 'react';
import {connect} from 'react-redux';

class Eliminar extends React.Component {
    constructor(props) {
        super(props);
    
         this.eliminar = this.eliminar.bind(this);
    }


    eliminar(){
        this.props.dispatch({
            type:'NETEJA_ELEMENTS',
           });
    }

    render() { 
        return ( 
        <>
            <button onClick={this.eliminar} >Borrar</button>
        </>
         );
    }
}
 

export default connect()(Eliminar);
  
  